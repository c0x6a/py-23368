Simple Project
---

Simple Django Project to test PyCharm's issue **PY-23368**: https://youtrack.jetbrains.com/issue/PY-23368

* file: `apps/home/fixtures/home-pycharm.json` was generated using Pycharm's `manage.py` console.
* file: `apps/home/fixtures/home-terminal.json` was generated using linux terminal with `python manage.py dumpdata` command.

There's an error when trying to load data using django's `loaddata` command with first file, because PyCharm adds some extra string at the end of the file.
