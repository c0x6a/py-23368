# coding=utf-8
from django.db import models


class SimpleObject(models.Model):
    """Simple object to test a CRU flow"""
    some_string = models.CharField(max_length=250)
    some_text = models.TextField()
    some_integer_number = models.IntegerField()

    def __str__(self):
        return "{id}: {string}. {text}, {number}".format(
            id=self.id, string=self.some_string, text=self.some_text,
            number=self.some_integer_number)
