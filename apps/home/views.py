# coding=utf-8
from django.urls.base import reverse_lazy
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.list import ListView

from apps.home.forms import SimpleObjectForm
from apps.home.models import SimpleObject


class HomeView(ListView):
    model = SimpleObject
    template_name = 'index.html'


class CreateSimpleObjectView(CreateView):
    form_class = SimpleObjectForm
    template_name = 'add_edit.html'
    success_url = reverse_lazy('home:index')


class UpdateSimpleObjectView(UpdateView):
    model = SimpleObject
    form_class = SimpleObjectForm
    template_name = 'add_edit.html'
    success_url = reverse_lazy('home:index')
