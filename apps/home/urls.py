# coding=utf-8
from django.conf.urls import url

from apps.home.views import HomeView, CreateSimpleObjectView, UpdateSimpleObjectView

urlpatterns = [
    url(r'^$', HomeView.as_view(), name='index'),
    url(r'^create/$', CreateSimpleObjectView.as_view(), name='create'),
    url(r'^update/(?P<pk>\d+)/$', UpdateSimpleObjectView.as_view(), name='update'),
]
