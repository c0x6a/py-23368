# coding=utf-8
from django import forms

from apps.home.models import SimpleObject


class SimpleObjectForm(forms.ModelForm):
    class Meta:
        model = SimpleObject
        fields = ['some_string', 'some_text', 'some_integer_number']
